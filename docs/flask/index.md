# Mise en place d'un serveur Flask

## L'algorithme de César
On rappelle la fonction  `cesar` mettant en \oe uvre la méthode de César pour crypter un texte :

```python linenums="1"
def cesar(phrase : str, cle : int) -> str:
    """ 
    Cette fonction retourne la chaîne de caractères obtenue par la 
    méthode de César appliquée sur la chaîne phrase avec la clé cle
    param phrase (str) : chaîne de caractères à crypter, sans 
                         caractères spéciaux, ni ponctuation 
                         ou accents, les espaces sont conservés
    param cle (int) : clé utilisée par la méthode de César
    return (str) : chaîne de caractères résultat du codage
    """
    phrase_crypte = ""
    for caractere in phrase:
        valeur_caractere = ord(caractere)
        if valeur_caractere != 32: # Espace
            # Il y a 58 caractères de A à z dans la table ASCII
            valeur_crypte = (valeur_caractere + cle - 65) % 58 + 65
        else:
            valeur_crypte = 32
        phrase_crypte = phrase_crypte + chr(valeur_crypte)
    return phrase_crypte
```

Voici deux exemples d'appels de cette fonction :    

    >>> cesar("Bonjour tout le monde", 10)
    LyxtyEB DyED vo wyxno
    >>> cesar("LyxtyEB DyED vo wyxno", -10)
    Bonjour tout le monde

## Création de la page HTML et du formulaire
On crée le répertoire Serveur avec l'arborescence suivante :

<center>
![arborescence](img/img1.png){width=25%}
</center>


Le fichier `formulaire.html` est le suivant :

```html linenums="1"
<!DOCTYPE html>
<html>
	<head>
		<title>Ma première page dynamique avec Flask</title>
		<meta name="author" content="Éric ROUGIER" >
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	</head>

<body style="background-color:lightgray ;">

<div style="text-align: center;"><b><i><h1>Codage avec la méthode de César</h1></i></b></div>

<form>
Saisir ci-dessous le texte à crypter : <br>
	<textarea rows=4 cols=40>
		Le texte n a ni caractere de 
	ponctuation ni caractere speciaux mais eventuellement des 
	espaces et des MAJUSCULES 
	</textarea><br>
Choisir une clé (un entier compris entre 1 et 26) : <br>
<input type = "text" value="15" maxlength="2"><br>
<button>Crypter</button>
</form>
</body>
</html>
```

Voici le rendu :

<center>
![Rendu](img/img2.png){width=70%}
</center>

## Mise en route du serveur Flask
Pour permettre la communication entre la page HTML (`formulaire.html`) et la fonction Python (`cesar`), nous allons mettre en route un serveur Flask. Flask est un framework de développement web en Python.
	
- Dans le répertoire `Serveur`, créer le fichier `index.py` suivant :

```python linenums="1"
from flask import Flask, render_template

# On cré une instance de la classe Flask : un serveur web
app = Flask(__name__)

# On décrit la route par défaut du serveur (racine du serveur)
@app.route("/")
def index():
    return render_template("formulaire.html")

if __name__ == "__main__":
	 # Lancement du serveur
     app.run(debug = True, host = "serveur-pgdg.net", port = "5000")  # Mode debug pour commencer
                                                                      # host = "localhost" en local
                                                                    # votre port personnel vous sera précisé
```

-  Exécuter ce fichier à partir d'un terminal :

<center>
![lancement du serveur](img/img5.png){ width=70% }
</center>

- Saisir l'adresse suivante dans votre navigateur : [http://serveur-pgdg.net:5000/](http://serveur-pgdg.net:5000/) pour un premier aperçu (vous indiquerez le numéro du port qui vous a été attribué).

## Interaction Formulaire - Serveur
Nous allons utiliser un formulaire de type `POST` pour transmettre les données du formulaire au serveur.

- Ajouter le paramètre `method = "POST` à la balise `form` du fichier `formulaire.html`.
 
Ajouter également l'adresse du serveur à suivre pour traiter ce formulaire à l'aide du paramètre `action = "/crypter/"` (cette adresse sera définie par la suite dans le fichier `index.py`) :

```html linenums="13"
<form action = "/crypter/" method = "POST">
...
</form>
```
- Ajouter également respectivement aux balises `textarea` et `input` les paramètres `name = "texte"` et `name = "cle"`. Le type de la balise `button` est `type = "submit"`.

Le serveur est maintenant prêt à recevoir les données du formulaire. Il reste à mettre en place la "route" vers l'adresse "`/crypter/`" du serveur.

## Traitement des données du formulaire
On suppose maintenant que la fonction `cesar` est codée dans un fichier nommé `fonctions.py` qui se trouve dans à la racine du répertoire `Serveur`.

- Ajouter ou modifier les lignes suivantes au fichier `index.py` déjà créé :

```python
# On ajoute l'import de la méthode request
from flask import Flask, render_template, request
from fonctions import cesar

...

@app.route("/crypter/", methods = ['POST'])
def crypter():
    texte = request.form['texte']
    cle = int(request.form['cle'])
    return cesar(texte, cle)

...
```

- Arrêter le serveur lancé précédemment par <span class="keys">
  <kbd class="key-ctrl">Ctrl</kbd>
  <span>+</span>
  <kbd class="key-alt">C</kbd>
</span> (dans le terminal) puis le redémarrer. Après avoir rechargé la page et cliqué sur le bouton, on constate le fonctionnement et l'affichage du résultat dans une page vide comme ci-dessous :
<center>
![](img/img6.png){width=100%} 
</center>

## Affichage du résultat dans une page HTML
- On cré une page `affichage.html`, dans le répertoire `templates` sur le modèle de la page \texttt{formulaire.html}. 

- On modifie la fonction `crypter` dans le fichier `index.py` ainsi :
```python
def crypter():
    texte = request.form['texte']
    cle = int(request.form['cle'])
    return render_template("affichage.html", texte = cesar(texte, cle))
```

- On intègre ce qui est envoyé par le codage `{texte}`.  Ici on aura le texte crypté dans une balise ``<h2>` :
```html linenums="1"
<!DOCTYPE html>
<html>
<head>
<title>Ma première page dynamique avec Flask</title>
<meta http-equiv = "content-type" content = "text/html; charset = UTF-8">
</head>
<body style = "background-color:lightgray ;">

<div style = "text-align: center;">
    <b><i><h1>Codage avec la méthode de César</h1></i></b>
</div>
    <h2>{{texte}}</h2><br>
    <input type = "button" onclick = "javascript:location.href = '/'" value = "Retour"/>
</body>
</html>
```

## Intégration de CSS et Javascript
On peut intégrer des feuilles style CSS et des scripts Javascript, les fichiers contenant le code doivent se trouver dans un répertoire nommé `static` :
<center>
![](img/img7.png) 
</center>
Ces deux fichiers seront incorporés de la manière suivante dans la balise \texttt{<head>} du code HTML :
```html
...
<head>
	<link href = "/static/style.css" rel = "stylesheet" type = "text/css" />
	<script src = "/static/scripts.js"></script>
</head>
...
```